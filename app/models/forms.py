
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField
from wtforms.validators import DataRequired


class LoginForm(FlaskForm):
	username = StringField("username", validators=[DataRequired()])
	password = PasswordField("password", validators=[DataRequired()])
	remenber_me = BooleanField("remember_me")

		
class CreateForm(FlaskForm):
	username = StringField("username", validators=[DataRequired()])
	password = PasswordField('password', validators=[DataRequired()])
	name = StringField('name', validators=[DataRequired()])
	email = StringField('email', validators=[DataRequired()])
	address = StringField('address', validators=[DataRequired()])
	phone = StringField('phone', validators=[DataRequired()])

