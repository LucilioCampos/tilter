from app import db


class Client(db.Model):
    __tablename__ = "customers"

    customer_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    first_name = db.Column(db.String, nullable=False)
    last_name = db.Column(db.String)
    email = db.Column(db.String, unique=True, nullable=False)
    address_id = db.Column(db.Integer, db.ForeignKey('address.id'))
    active = db.Column(db.Boolean, default=0, nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    last_update = db.Column(db.TIMESTAMP)

    address = db.relationship('Address', foreign_keys=address_id)

    @property
    def is_authenticated(self):
        return False

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return True

    def get_id(self):
        return self.id

    def __init__(self, first_name, last_name, email, active):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.active = active

    def __repr__(self):
        return '<Client: %r>' %self.first_name



