from flask import render_template, flash, redirect, url_for
from app import app, db, lm
from app.models.usuario import User
from flask_login import login_required
from app.models.forms import CreateForm


@app.route('/bootest')
def bootest():

    return render_template('bootest.html')
