from flask import render_template, flash, redirect, url_for
from app import app, db, lm
from app.models.usuario import User
from flask_login import login_user, logout_user, login_required, current_user
from app.models.forms import LoginForm


@lm.user_loader
def load_user(id):
    return User.query.filter_by(id=id).first()


@app.route("/home")
def home():
    return render_template('index.html')


@app.route("/")
def index():
    if not current_user.is_authenticated:
        redirect(url_for('home'))
    else:
        redirect(url_for('home'))
    return render_template("index.html")


@app.route("/login", methods=['POST', 'GET'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user and user.password == form.password.data:
            login_user(user)
            flash("Logged in")
            return redirect(url_for("home"))
        else:
            flash("Invalid Login")

    return render_template('login.html', form=form)


@app.route('/logout')
@login_required
def logout():
    logout_user()
    flash("Logout")
    return redirect(url_for('login'))


@app.route("/test")
def test():
    return render_template('bootest.html')
