from flask import render_template, flash, redirect, url_for
from app import app, db, lm
from app.models.usuario import User
from flask_login import login_required
from app.models.forms import CreateForm


@app.route("/usuarios")
@login_required
def usuarios():
	usuario = db.session.query(User).all()
	return render_template('usuarios.html', usuario=usuario)


@app.route('/create', methods=['GET', 'POST'])
@login_required
def create():
	form = CreateForm()
	if form.validate_on_submit():
		u_user = str(form.username.data)
		u_pass = str(form.password.data)
		u_name = str(form.name.data)
		u_email = str(form.email.data)
		u_address = str(form.address.data)
		u_phone = str(form.phone.data)
		user = User.query.filter_by(username=u_name).first()
		if user:
			flash("Usuario ja existe!")
			return redirect(url_for("create"))
		else:
			usuario = User(u_user, u_pass, u_name, u_email)
			db.session.add(usuario)
			db.session.commit()
			flash("Usuario criado com sucesso!")
			return redirect(url_for('home'))

	return render_template('create.html', form=form)
	


