from flask import render_template

from app import app


@app.route('/configuracoes')
def config():
    return render_template('config.html')
